--[[
https://coronalabs.com/blog/2014/09/30/tutorial-how-to-shuffle-table-items/
--]]

require 'torch'
require 'nn'
require 'nngraph'
require 'optim'
require 'lfs'
require 'csvigo'
require 'math'
require 'ffmpeg'

local FootballCnnData = {}
FootballCnnData.__index = FootballCnnData

function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end

math.randomseed( os.time() )

local function shuffleTable( t )
    local rand = math.random 
    assert( t, "shuffleTable() expected a table, got nil" )
    local iterations = #t
    local j
    
    for i = iterations, 2, -1 do
        j = rand(i)
        t[i], t[j] = t[j], t[i]
    end
end


function FootballCnnData.create(data_dir, csv_file)
	torch.setdefaulttensortype('torch.FloatTensor')
	local self = {}
	setmetatable(self, CharSplitLMMinibatchLoader)
	
	local ylabels = {}
	local m = csvigo.load({path = csv_file, mode = "large"})
	for i = 1, #m do
		line = mysplit(m[i][1])
		if line[3] == 'p' then
			table.insert(ylabels, 1.0)
		end
		if line[3] == 'r' then
			table.insert(ylabels, 2.0)
		end
	end
	
	file_name = data_dir .. 1 - 1 .. '_' .. 0 .. '.data' 
	test_file = torch.DiskFile(file_name, 'r')
	test_file:binary()

	test_storage = torch.FloatStorage(1024)
	test_size = test_file:readFloat(test_storage)
	test_tensor = torch.Tensor(test_storage):cuda()
	
	
	self.x_data = {}
	self.y_data = {}
	
	local r_data = {}
	local p_data = {}
	for i = 1,#ylabels do
		x_instance = {}
		--
		for j = 0,119, 4 do
			local file_name = data_dir .. i - 1 .. '_' .. j .. '.data' 
			local test_file = torch.DiskFile(file_name, 'r')
			test_file:binary()

			local test_storage = torch.FloatStorage(1024)
			test_size = test_file:readFloat(test_storage)
			test_tensor = torch.Tensor(test_storage)
			
			table.insert(x_instance, test_tensor)
			--[[my_label = torch.Tensor(1):cuda()
			my_label[1] = ylabels[i]
			table.insert(current_label, my_label)
			--]]
			test_file:close()
		end
		--table.insert(self.y_data, current_label)
		if ylabels[i] == 1 then
			table.insert(p_data, x_instance)
		else
			table.insert(r_data, x_instance)
		end
		print('finished: ' .. i .. ' / ' .. #ylabels)
	end
	
	shuffleTable(r_data)
	shuffleTable(p_data)
	
	for i = 1,#r_data do
		table.insert(self.x_data, r_data[i])
		
		table.insert(self.x_data, p_data[i])
		r_label = {}
		p_label = {}
		for j = 1,#r_data[1] do
			my_label = torch.Tensor(1):cuda()
			my_label[1] = 2.0
			table.insert(r_label, my_label)
			other_label = torch.Tensor(1):cuda()
			other_label[1] = 1.0
			table.insert(p_label, other_label)
		end
		table.insert(self.y_data, r_label)
		table.insert(self.y_data, p_label)
	end
			
	
	return self
	
end

function FootballCnnData.create_frame_seq(data_dir, csv_file)
	torch.setdefaulttensortype('torch.FloatTensor')
	local self = {}
	setmetatable(self, CharSplitLMMinibatchLoader)
	
	local ylabels = {}
	local video_names = {}
	local m = csvigo.load({path = csv_file, mode = "large"})
	for i = 1, #m do
		line = mysplit(m[i][1])
		data_instance = {}
			
		if line[3] == 'p' then
			table.insert(video_names, line[1])
		
			table.insert(data_instance, 1.0)
			
			if line[2] == 'l' then
				table.insert(data_instance, 3.0)
			else
				table.insert(data_instance, 4.0)
			end
			
			table.insert(ylabels, data_instance)
		end
		if line[3] == 'r' then
			table.insert(video_names, line[1])
		
			table.insert(data_instance, 2.0)
			if line[2] == 'l' then
				table.insert(data_instance, 3.0)
			else
				table.insert(data_instance, 4.0)
			end
			
			table.insert(ylabels, data_instance)
		end
	end
	--[[
	print(video_names[1])
	local vid = ffmpeg.Video({['path'] = video_names[1], ['length'] = 200, ['silent'] = true})
	
	print(vid.nframes)
	print(vid[1][1])
	
	print(vid[1][1][1][1][1])
	print(vid[1][1][2][1][1])
	print(vid[1][1][3][1][1])
	local a = image.rgb2y(vid[1][1])
	print(a[1][1][1])
end
--]]

	self.dec_out = {}
	self.dec_in = {}
	self.enc_in = {}
	
	local r_data = {}
	local p_data = {}
	for i = 1,100 do
		local x_instance = {}
		local vid = ffmpeg.Video({['path'] = video_names[i], ['length'] = 200, ['silent']=true})
		print(video_names[i])
		for j = 1,vid.nframes, 4 do
			local g = image.rgb2y(vid[1][j])
			table.insert(x_instance, g)
		end
		
		xy = {}
		table.insert(xy, x_instance)
		table.insert(xy, ylabels[i])
		
		if ylabels[i][1] == 1 then
			table.insert(p_data, xy)
		else
			table.insert(r_data, xy)
		end
		
		print('finished: ' .. i .. ' / ' .. #ylabels)
	end
	
	shuffleTable(r_data)
	shuffleTable(p_data)
	
	for i = 1,#r_data do
		table.insert(self.enc_in, r_data[i][1])
		
		
		table.insert(self.enc_in, p_data[i][1])
		
		
		local r_out = {}
		local r_in = {}
		local stop = torch.Tensor(1):cuda()
		stop[1] = 1
		table.insert(r_in, stop)
		
		for j = 1,#r_data[i][2] do
			
			local word = torch.Tensor(1):cuda()
			word[1] = r_data[i][2][j] + 1
			table.insert(r_out, word)
			table.insert(r_in, word:clone())
		end
		
		table.insert(r_out, stop:clone())
		table.insert(self.dec_out, r_out)
		table.insert(self.dec_in, r_in)
		
		local p_out = {}
		local p_in = {}
		
		table.insert(p_in, stop:clone())
		
		for j = 1,#p_data[i][2] do
			
			local word = torch.Tensor(1):cuda()
			word[1] = p_data[i][2][j] + 1
			table.insert(p_out, word)
			table.insert(p_in, word:clone())
		end
		
		table.insert(p_out, stop:clone())
		table.insert(self.dec_out, p_out)
		table.insert(self.dec_in, p_in)
		
	end
			
	
	return self
	
					
end
--]]
	
	

function FootballCnnData.create_seq(data_dir, csv_file)
	torch.setdefaulttensortype('torch.FloatTensor')
	local self = {}
	setmetatable(self, CharSplitLMMinibatchLoader)
	
	local ylabels = {}
	
	local m = csvigo.load({path = csv_file, mode = "large"})
	for i = 1, #m do
		line = mysplit(m[i][1])
		data_instance = {}
		if line[3] == 'p' then
			table.insert(data_instance, 1.0)
			if line[2] == 'l' then
				table.insert(data_instance, 3.0)
			else
				table.insert(data_instance, 4.0)
			end
			
			table.insert(ylabels, data_instance)
		end
		if line[3] == 'r' then
			table.insert(data_instance, 2.0)
			if line[2] == 'l' then
				table.insert(data_instance, 3.0)
			else
				table.insert(data_instance, 4.0)
			end
			
			table.insert(ylabels, data_instance)
		end
	end
	
	
	file_name = data_dir .. 1 - 1 .. '_' .. 0 .. '.data' 
	test_file = torch.DiskFile(file_name, 'r')
	test_file:binary()

	test_storage = torch.FloatStorage(1024)
	test_size = test_file:readFloat(test_storage)
	test_tensor = torch.Tensor(test_storage):cuda()
	
	
	self.dec_out = {}
	self.dec_in = {}
	self.enc_in = {}
	
	local r_data = {}
	local p_data = {}
	for i = 1,#ylabels do
		x_instance = {}
		--
		for j = 0,119, 4 do
			local file_name = data_dir .. i - 1 .. '_' .. j .. '.data' 
			local test_file = torch.DiskFile(file_name, 'r')
			test_file:binary()

			local test_storage = torch.FloatStorage(1024)
			test_size = test_file:readFloat(test_storage)
			test_tensor = torch.Tensor(test_storage):cuda()
			
			table.insert(x_instance, test_tensor)
			--[[my_label = torch.Tensor(1):cuda()
			my_label[1] = ylabels[i]
			table.insert(current_label, my_label)
			--]]
			test_file:close()
		end
		--table.insert(self.y_data, current_label)
		
		xy = {}
		table.insert(xy, x_instance)
		table.insert(xy, ylabels[i])
		
		if ylabels[i][1] == 1 then
			table.insert(p_data, xy)
		else
			table.insert(r_data, xy)
		end
		
		print('finished: ' .. i .. ' / ' .. #ylabels)
	end
	
	shuffleTable(r_data)
	shuffleTable(p_data)
	
	for i = 1,#r_data do
		table.insert(self.enc_in, r_data[i][1])
		
		
		table.insert(self.enc_in, p_data[i][1])
		
		
		local r_out = {}
		local r_in = {}
		local stop = torch.Tensor(1):cuda()
		stop[1] = 1
		table.insert(r_in, stop)
		
		for j = 1,#r_data[i][2] do
			
			local word = torch.Tensor(1):cuda()
			word[1] = r_data[i][2][j] + 1
			table.insert(r_out, word)
			table.insert(r_in, word:clone())
		end
		
		table.insert(r_out, stop:clone())
		table.insert(self.dec_out, r_out)
		table.insert(self.dec_in, r_in)
		
		local p_out = {}
		local p_in = {}
		
		table.insert(p_in, stop:clone())
		
		for j = 1,#p_data[i][2] do
			
			local word = torch.Tensor(1):cuda()
			word[1] = p_data[i][2][j] + 1
			table.insert(p_out, word)
			table.insert(p_in, word:clone())
		end
		
		table.insert(p_out, stop:clone())
		table.insert(self.dec_out, p_out)
		table.insert(self.dec_in, p_in)
		
	end
			
	
	return self
	
end



--function CnnFrameOutput.create(

return FootballCnnData
--[[local test_loader = FootballCnnData.create('/scratch/tfiez/football_play_recognition/small_test/data/', '/scratch/tfiez/direction_detection/football-play-direction-detection/gt_annotations/play_data.csv')
]]
