
require 'cutorch'
require 'cunn'
cutorch.setDevice(1)
require 'rnn'
require 'math'
local CnnOutput = require 'CnnFrameOutput'









function forwardConnect(encLSTM, decLSTM, inputSeqLen)
  decLSTM.userPrevOutput = nn.rnn.recursiveCopy(decLSTM.userPrevOutput, encLSTM.outputs[inputSeqLen])
  decLSTM.userPrevCell = nn.rnn.recursiveCopy(decLSTM.userPrevCell, encLSTM.cells[inputSeqLen])
end

function forwardTableConnect(encoders, decoders, inputSeqLen)
	for i = 1,#encoders do
		decoders[i].userPrevOutput = nn.rnn.recursiveCopy(decoders[i].userPrevOutput, encoders[i].outputs[inputSeqLen])
		decoders[i].userPrevCell = nn.rnn.recursiveCopy(decoders[i].userPrevCell, encoders[i].cells[inputSeqLen])
	end
end

function backwardConnect(encLSTM, decLSTM)
  encLSTM.userNextGradCell = nn.rnn.recursiveCopy(encLSTM.userNextGradCell, decLSTM.userGradPrevCell)
  encLSTM.gradPrevOutput = nn.rnn.recursiveCopy(encLSTM.gradPrevOutput, decLSTM.userGradPrevOutput)
end

function backwardTableConnect(encoders, decoders)
	for i = 1,#encoders do
		encoders[i].userNextGradCell = nn.rnn.recursiveCopy(encoders[i].userNextGradCell, decoders[i].userGradPrevCell)
		encoders[i].gradPrevOutput = nn.rnn.recursiveCopy(encoders[i].gradPrevOutput, decoders[i].userGradPrevOutput)
	end
end


local function validation_accuracy(enc_in, dec_in, dec_out, enc, dec, encLSTM, decLSTM, start_index, end_index)
	cm = torch.Tensor(2, 2):zero()
	predictions = torch.Tensor(2, 2):zero()
	
	better_cm = torch.Tensor(5, 5):zero()
	
	--cm_frame = torch.Tensor(#input[1], 2, 2):zero()
	local stop = 1
	for i = start_index,end_index do
		local encOut = enc:forward(enc_in[i])
		forwardTableConnect(encLSTM, decLSTM, #enc_in[i])
		
		
		local prev_in = torch.Tensor(1):cuda()
		local my_guess = {}
		prev_pred = 1
		local length = 0
		repeat
			local d_in = {}
			
			prev_in[1] = prev_pred
			table.insert(d_in, prev_in)
			--print(d_in)
			local decOut = dec:forward(d_in)
			--print(decOut)
			my_max = -100
			my_max_index = 0
			
			for j = 1,decOut[1][1]:size()[1] do
				if my_max_index == 0 or decOut[1][1][j] > my_max then
					my_max_index = j
					my_max = decOut[1][1][j]
				end
			end
			
			prev_pred = my_max_index
			
			table.insert(my_guess, prev_pred)
			length = length + 1
		until (prev_pred == stop or length > 4)
		--[[
		print(i)
		for j = 1,#my_guess do
			io.write(my_guess[j], " ")
		end
		print("")
		for j = 1,#dec_out[i] do
			io.write(dec_out[i][j][1], " ")
		end
		print("")
		--]]
		enc:forget()
		dec:forget()
		
		--cm[dec_out[i][1][1] - 1][my_guess[1] - 1] = cm[dec_out[i][1][1] - 1][my_guess[1] - 1] + 1
		
		local lowest_length = #dec_out[i]
		if (lowest_length > #my_guess) then
			lowest_length = #my_guess
		end
		
		if (i < 5) then
			for j = 1,#my_guess do
				io.write(my_guess[j], " ")
			end
			print("")
			for j =1,#dec_out[i] do
				io.write(dec_out[i][j][1], " ")
			end
			print("")
		end
			
		for j = 1,lowest_length do
			better_cm[dec_out[i][j][1]][my_guess[j]] = better_cm[dec_out[i][j][1]][my_guess[j]] + 1
		end
		--]]
	end
	
	for i = 1,5 do
		for j = 1,5 do
			io.write(better_cm[i][j], " ")
		end
		print("")
	end
	
	for i = 1,2 do
		for j =1,2 do
			io.write(cm[i][j], " ")
		end
		print("")
	end
end
		
		

local function main()
	cutorch.setDevice(1)
	lr = 0.005
	updateInterval = 48;
	loader = CnnOutput.create_seq('/scratch/tfiez/football_play_recognition/small_test/data/', '/scratch/tfiez/direction_detection/football-play-direction-detection/gt_annotations/play_data.csv')
	
	local encoders = {}
	local decoders = {}
	
	local enc = nn.Sequential()
	local encLSTM = nn.FastLSTM(1024, 256)
	enc:add(nn.Sequencer(encLSTM))
	local otherEnc = nn.FastLSTM(256, 256)
	enc:add(nn.Sequencer(otherEnc))
  	enc:add(nn.SelectTable(-1))
	
	table.insert(encoders, encLSTM)
	table.insert(encoders, otherEnc)
	
	local dec = nn.Sequential()
	dec:add(nn.Sequencer(nn.LookupTable(5, 256)))
	local decLSTM = nn.FastLSTM(256, 256)
	
	dec:add(nn.Sequencer(decLSTM))
  
	local otherDec = nn.FastLSTM(256, 256)
	dec:add(nn.Sequencer(otherDec))
	
	table.insert(decoders, decLSTM)
	table.insert(decoders, otherDec)
	
	dec:add(nn.Sequencer(nn.Linear(256, 5)))
	dec:add(nn.Sequencer(nn.LogSoftMax()))
	
	criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
	
	enc:cuda()
	dec:cuda()
	
	criterion:cuda()
	
	
	local encParams, encGradParams = enc:getParameters()
	local decParams, decGradParams = dec:getParameters()

	print(#loader.dec_out)
	print(#loader.dec_out[1])
	print(loader.dec_out[1][1]:size())
	
	collectgarbage()
	
	for module_index, module in ipairs(enc:listModules()) do
		print(module)
	end
	
	for module_index, module in ipairs(dec:listModules()) do
		print(module)
	end
	
	
	for epoch = 1,100 do
		for it = 1, 100, 1 do
			print(it)
			index = math.random(1,1000 - updateInterval)
			enc:zeroGradParameters()
			dec:zeroGradParameters()
			local avg_loss = 0
			for i = index, index+updateInterval, 1 do

				local e_in = loader.enc_in[i]
				local d_in = loader.dec_in[i]
				local d_out = loader.dec_out[i]
				local encOut = enc:forward(e_in)
				--forwardConnect(encLSTM, decLSTM, #e_in)
				forwardTableConnect(encoders, decoders, #e_in)
				local decOut = dec:forward(d_in)
				--[[
				for j = 1,#decOut do
					for o = 1,decOut[j]:size()[1] do
						for stuff = 1,decOut[j]:size()[2] do
							print(math.exp(decOut[j][o][1]))
						end
					end
				end
				--]]
				--print(d_out)
				local Edec = criterion:forward(decOut, d_out)
				avg_loss = avg_loss + Edec
	-- Backward pass
				local gEdec = criterion:backward(decOut, d_out)
				dec:backward(d_in, gEdec)
				--backwardConnect(encLSTM, decLSTM)
				backwardTableConnect(encoders, decoders)
				local zeroTensor = torch.Tensor(2):zero():cuda()
				enc:backward(e_in, zeroTensor)
				enc:forget()
				dec:forget()
			end
			avg_loss = avg_loss / updateInterval
			print(it .. " " .. avg_loss)
			dec:updateParameters(lr)
			enc:updateParameters(lr)
		end
		
		validation_accuracy(loader.enc_in, loader.dec_in, loader.dec_out, enc, dec, encoders, decoders, 1, 1000)
		validation_accuracy(loader.enc_in, loader.dec_in, loader.dec_out, enc, dec, encoders, decoders, 1001, #loader.enc_in)
		--[[
		if i == 11 then
			lr = 0.00001
		end
		--]]
	end
	
end

main()
	

