require 'cutorch'
require 'cunn'
cutorch.setDevice(1)
require 'rnn'
require 'math'
local CnnOutput = require 'CnnFrameOutput'

local function test_accuracy(input, output, rnn)

	cm = torch.Tensor(2, 2):zero()
	cm_frame = torch.Tensor(input:size()[2], 2, 2):zero()
	for i = 1,#input do
		pred = rnn:forward(input[i])
		rnn:forget()
		local total = 0
		for j = 1, #pred do
			if (math.exp(pred[j][1]) < 0.5) then
				total = total + 1
				cm_frame[j][math.output[i][1][1]][2] = cm_frame[j][math.output[i][1][1]][2] + 1
			else
				cm_frame[j][math.output[i][1][1]][1] = cm_frame[j][math.output[i][1][1]][1] + 1
			end
		end
		local cf_index = 1
		if (total > 60) then
			cf_index = 2
		end
		
		cm[math.floor(output[i][1][1])][cf_index] = cm[math.floor(output[i][1][1])][cf_index] + 1
		
	end
	
	print(cm[1][1] .. ' ' .. cm[1][2])
	print(cm[2][1] .. ' ' .. cm[2][2])
	
	print('Frame accuracies')
	for i = 1,cm_frame:size()[1] do
		print('Frame: ' .. i)
		print(cm_frame[i][1][1] .. ' ' .. cm_frame[i][1][2])
		print(cm_frame[i][2][1] .. ' ' .. cm_frame[i][2][2])
	end
end

local function validation_accuracy(input, output, rnn, start_index, end_index)
	cm = torch.Tensor(2, 2):zero()
	predictions = torch.Tensor(2, 2):zero()
	
	cm_frame = torch.Tensor(#input[1], 2, 2):zero()
	for i = start_index,end_index do
		pred = rnn:forward(input[i])
		
		local total = 0
		for j = 1, #pred do
			if (math.exp(pred[j][1]) < 0.5) then
				total = total + 1
				predictions[math.floor(output[i][1][1])][2] = predictions[math.floor(output[i][1][1])][2] + 1
				cm_frame[j][math.floor(output[i][1][1])][2] = cm_frame[j][math.floor(output[i][1][1])][2] + 1
			else
				predictions[math.floor(output[i][1][1])][1] = predictions[math.floor(output[i][1][1])][1] + 1
				cm_frame[j][math.floor(output[i][1][1])][1] = cm_frame[j][math.floor(output[i][1][1])][1] + 1
			end
		end
		local cf_index = 1
		if (total > (#pred / 2)) then
			cf_index = 2
		end
		
		cm[math.floor(output[i][1][1])][cf_index] = cm[math.floor(output[i][1][1])][cf_index] + 1
		
	end
	print("Confusion Matrix")
	print(cm[1][1] .. ' ' .. cm[1][2])
	print(cm[2][1] .. ' ' .. cm[2][2])
	print("All predictions")
	print(predictions[1][1] .. ' ' .. predictions[1][2])
	print(predictions[2][1] .. ' ' .. predictions[2][2])
	print('Frame accuracies')
	for i = 1,cm_frame:size()[1] do
		print('Frame: ' .. i)
		print(cm_frame[i][1][1] .. ' ' .. cm_frame[i][1][2])
		print(cm_frame[i][2][1] .. ' ' .. cm_frame[i][2][2])
	end	
end

local function main()
	cutorch.setDevice(1)
	lr = 0.0001
	updateInterval = 20;
	loader = CnnOutput.create('/scratch/tfiez/football_play_recognition/small_test/data/', '/scratch/tfiez/direction_detection/football-play-direction-detection/gt_annotations/play_data.csv')
	
	
	
	
	
	rnn = nn.Sequential()
  
	rnn:add(nn.Sequencer(nn.FastLSTM(1024, 256)))
  
	rnn:add(nn.Sequencer(nn.FastLSTM(256, 512)))

	rnn:add(nn.Sequencer(nn.Linear(512, 2)))
	rnn:add(nn.Sequencer(nn.LogSoftMax()))
	
	criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
	test_criterion = nn.ClassNLLCriterion()
	
	rnn:cuda()
	
	criterion:cuda()
	collectgarbage()
	
	--test_accuracy(loader.x_data, loader.y_data, rnn)
	print("Training Accuracy")
	validation_accuracy(loader.x_data, loader.y_data, rnn, 1, #loader.x_data)
	print("Validation Accuracy")
	validation_accuracy(loader.x_data, loader.y_data, rnn, 1, #loader.x_data)
	for epoch = 1,10 do
		for it = 1, 100, 1 do
			print(it)
			index = math.random(1,1000)
			rnn:zeroGradParameters()
			for i = index, index+updateInterval, 1 do

				local input = loader.x_data[i]
				local target = loader.y_data[i]
				local output = rnn:forward(input)
				local err = criterion:forward(output, target)
				local gradOutput = criterion:backward(output, target)
			
				rnn:backward(input, gradOutput)
				rnn:forget()
			end
			rnn:updateParameters(lr)
		end
		print("For epoch #" .. epoch)
		print("Training Accuracy")
		validation_accuracy(loader.x_data, loader.y_data, rnn, 1, 1000)
		print("Validation Accuracy")
		validation_accuracy(loader.x_data, loader.y_data, rnn, 1001, #loader.x_data)
	end
end

main()
