
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <fstream>
#include <caffe/caffe.hpp>
#include <algorithm>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;
using namespace cv;
using namespace caffe;



class CNNFeatExtractor {
	public:
		CNNFeatExtractor(const string & model_file,
                       const string& trained_file,
                       const string& mean_file,
                       const string& label_file);
                void ExtractLayerFeatures(vector<Mat> images, string data_dir, int layer_output);
	private:
		void SetMean(const string& mean_file);
		
		void WrapInputLayer(std::vector<vector<cv::Mat> > &input_channels, int image_num);

		void Preprocess(vector<Mat> images,
			  std::vector<vector<cv::Mat> > &input_channels);
			  
			  
		shared_ptr<Net<float> > net_;
		cv::Size input_geometry_;
		int num_channels_;
		cv::Mat mean_;
		std::vector<string> labels_;
}; 


CNNFeatExtractor::CNNFeatExtractor(const string& model_file,
                       const string& trained_file,
                       const string& mean_file,
                       const string& label_file)
{
#ifdef CPU_ONLY
	Caffe::set_mode(Caffe::CPU);
#else
	Caffe::set_mode(Caffe::GPU);
#endif

	net_.reset(new Net<float>(model_file, TEST));
	net_->CopyTrainedLayersFrom(trained_file);

	CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
	CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

	Blob<float>* input_layer = net_->input_blobs()[0];
	num_channels_ = input_layer->channels();
	cout << num_channels_ << endl;
	CHECK(num_channels_ == 3 || num_channels_ == 1)
	<< "Input layer should have 1 or 3 channels.";
	input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
	cout << input_geometry_ << endl;
	/* Load the binaryproto mean file. */
	SetMean(mean_file);

	/* Load labels. */
	std::ifstream labels(label_file.c_str());
	CHECK(labels) << "Unable to open labels file " << label_file;
	string line;
	while (std::getline(labels, line))
		labels_.push_back(string(line));

	Blob<float>* output_layer = net_->output_blobs()[0];
	/*
	vector<string> names = net_->layer_names();
	const vector< vector< Blob<float> *> > tops = net_->top_vecs();
	
	
	for (int i = 0; i < names.size(); i++)
		cout << i << " " << names[i] << endl;
	
	cout << names[20] << endl;
	vector<int> shape = tops[20][0]->shape();
	
	for (int i = 0; i < shape.size(); i++)
		cout << shape[i] << " ";
	cout << endl;
	/*CHECK_EQ(labels_.size(), output_layer->channels())
	<< "Number of labels is different from the output layer dimension.";
	*/
}

void CNNFeatExtractor::SetMean(const string& mean_file) {
	BlobProto blob_proto;
	ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

	/* Convert from BlobProto to Blob<float> */
	Blob<float> mean_blob;
	mean_blob.FromProto(blob_proto);
	cout << mean_blob.channels() << endl;
	CHECK_EQ(mean_blob.channels(), num_channels_)
		<< "Number of channels of mean file doesn't match input layer.";

	/* The format of the mean file is planar 32-bit float BGR or grayscale. */
	std::vector<cv::Mat> channels;
	float* data = mean_blob.mutable_cpu_data();
	for (int i = 0; i < num_channels_; ++i) {
		/* Extract an individual channel. */
		cv::Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
		channels.push_back(channel);
		data += mean_blob.height() * mean_blob.width();
	}

	/* Merge the separate channels into a single image. */
	cv::Mat mean;
	cv::merge(channels, mean);

	/* Compute the global mean pixel value and create a mean image
	* filled with this value. */
	cv::Scalar channel_mean = cv::mean(mean);
	mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
}

void CNNFeatExtractor::WrapInputLayer(std::vector<vector<cv::Mat> > &input_channels, int image_num) {
	Blob<float>* input_layer = net_->input_blobs()[0];

	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();
	for (int img = 0; img < image_num; img++) {
		for (int i = 0; i < input_layer->channels(); ++i) {
			cv::Mat channel(height, width, CV_32FC1, input_data);
			input_channels[img].push_back(channel);
			input_data += width * height;
		}
	}
}

void CNNFeatExtractor::Preprocess(vector<Mat> images,
                            vector<vector<cv::Mat> > &input_channels) {
/* Convert the input image to the input image format of the network. */
	for (int image_num = 0; image_num < images.size(); image_num++) {
		Mat img = images[image_num];	
		cv::Mat sample;
		if (img.channels() == 3 && num_channels_ == 1)
			cv::cvtColor(img, sample, CV_BGR2GRAY);
		else if (img.channels() == 4 && num_channels_ == 1)
			cv::cvtColor(img, sample, CV_BGRA2GRAY);
		else if (img.channels() == 4 && num_channels_ == 3)
			cv::cvtColor(img, sample, CV_BGRA2BGR);
		else if (img.channels() == 1 && num_channels_ == 3)
			cv::cvtColor(img, sample, CV_GRAY2BGR);
		else
			sample = img;

		cv::Mat sample_resized;

		if (sample.size() != input_geometry_)
			cv::resize(sample, sample_resized, input_geometry_);
		else
			sample_resized = sample;

		cv::Mat sample_float;
		if (num_channels_ == 3)
			sample_resized.convertTo(sample_float, CV_32FC3);
		else
			sample_resized.convertTo(sample_float, CV_32FC1);

		cv::Mat sample_normalized;
		cv::subtract(sample_float, mean_, sample_normalized);

		/* This operation will write the separate BGR planes directly to the
		* input layer of the network because it is wrapped by the cv::Mat
		* objects in input_channels. */
		cv::split(sample_normalized, input_channels[image_num]);

		/*CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
			== net_->input_blobs()[0]->cpu_data())
		<< "Input channels are not wrapping the input layer of the network.";*/
	}
}

void CNNFeatExtractor::ExtractLayerFeatures(vector<Mat> images,
		string data_dir, int layer_output)
{
	Blob<float>* cur_layer = net_->input_blobs()[0];
	
	Blob<float>* input_layer = net_->input_blobs()[0];
	input_layer->Reshape(images.size(), num_channels_,
		       input_geometry_.height, input_geometry_.width);

	net_->Reshape();
	
	std::vector<vector<cv::Mat> >input_channels;
	
	for (int i = 0; i< images.size(); i++) {
		vector<Mat> new_vec;
		input_channels.push_back(new_vec);
	}

	WrapInputLayer(input_channels, images.size());

	Preprocess(images, input_channels);


	net_->ForwardPrefilled();

	int net_data_index = 0;
	
	const vector< vector< Blob< float > * > > all_data = net_->bottom_vecs();
	
	const float *net_data = all_data[layer_output][0]->cpu_data();
	
	vector<int> template_shape;
	
	
	template_shape = all_data[layer_output][0]->shape();
	
	
	int size_needed = 1;
	for (int i = 1; i < template_shape.size(); i++)
		size_needed *= template_shape[i];
	
	cout << size_needed << endl;	
	
	for (int image_num = 0; image_num < images.size(); image_num++) {
		stringstream ss;
		ss << image_num << ".data";
		
		string output_name = data_dir + ss.str();
		
		int fd = open(output_name.c_str(), O_CREAT | O_TRUNC | O_WRONLY, S_IRWXU  | S_IRWXG | S_IRWXO);
		write(fd, &net_data[size_needed * image_num], size_needed * sizeof(float));
		close(fd);
		
		
	}
}

void read_video_names(string datafile, vector<string> &video_names, vector<int> &labels, vector<int> &mos_frame)
{
	ifstream csv_file(datafile.c_str());

	while (!csv_file.eof()) {
		string name, play_type, junk;
		int mos, stuff;
		
		csv_file >> name >> junk >> play_type >> mos >> stuff >> stuff;
		
		if (play_type.compare("k") == 0)
			continue;
			
		video_names.push_back(name);
		if (play_type.compare("p") == 0)
			labels.push_back(0);
		else
			labels.push_back(1);
		
		mos_frame.push_back(mos);
	}
	cout << video_names.size() << endl;

}


int main(int argc, char **argv)
{
	fstream config_file(argv[1]);
	string model_file, trained_file, mean_file, label_file;

	config_file >> model_file;
	config_file >> trained_file;
	config_file >> mean_file;
	config_file >> label_file;
	
	CNNFeatExtractor *Extractor = new CNNFeatExtractor(model_file, trained_file, mean_file, label_file);
	string datafile = "/scratch/tfiez/direction_detection/football-play-direction-detection/gt_annotations/play_data.csv";
	string datadir = "/scratch/tfiez/football_play_recognition/small_test/data/";
	
	vector<string> video_names;
	vector<int> labels;
	vector<int> mos_frame;
	
	read_video_names(datafile, video_names, labels, mos_frame);
	
	for (int vid_num = 0; vid_num < video_names.size(); vid_num++) {
		cout << vid_num << endl;
		cout << video_names[vid_num] << endl;
		VideoCapture cur_vid(video_names[vid_num]);
		
		//cout << cur_vid.get(CV_CAP_PROP_FRAME_COUNT) << " " << mos_frame[vid_num] << endl;
		//cout << video_names[vid_num] << endl;
		cur_vid.set(CV_CAP_PROP_POS_FRAMES, mos_frame[vid_num]);
		
		vector<Mat> image_list;
		for (int i = 0; i < 120; i++) {
			Mat frame;
			cur_vid >> frame;
			image_list.push_back(frame);
		}
		stringstream ss;
		ss << datadir << vid_num << "_";
		
		Extractor->ExtractLayerFeatures(image_list, ss.str(), 20);
		
	}

	return 0;
}






